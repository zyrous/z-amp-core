# z-amp-core Documentation

Welcome to the documentation section for the **z-amp-core** library. Web audio is a passion of ours and we want to ensure that developers are provided with the tools that they need to create some awesome experiences. If you have any ideas for the library or suggestions for how to improve it, please don't hesitate to get in touch. With that out of the way, let's get started!

## [Introduction](./INTRO.md)
Learn what **z-amp-core** is and what you can achieve with it.
## [Basic Concepts](./CONCEPTS.md)
Get a handle on the terminology of **z-amp-core** and how it all fits together.
## [Adding **z-amp-core** to a Website](./INSTALLATION.md)
Install the core library on your website.
## [Creating an Audio Pipeline](./INITIALISING.md)
Start adding functionality with a custom audio pipeline.
## [Listening for Events](./EVENTS.md)
Find out how to receive notifications when events occur within the framework.
## [**z-amp-core** Modules](./MODULES.md)
Learn what each core module does and how to use it.
## [Creating Themes](./CREATE_THEME.md)
Create and distribute your own custom theme for others to use.
